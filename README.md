# Dicom Visualization

Dicom Visualization module provides file field widget and formatter to allow use Dicom visualization with the standard File field. DICOM stands for Digital Imaging and Communications in Medicine. It is a widely used standard in the medical industry for storing, transmitting, and sharing medical images and related information. DICOM enables interoperability between different medical devices and systems, allowing for the exchange of medical images and patient data among healthcare providers. The DCM file extension is used for DICOM files.

Using Dicom Visualization module you will not need to use another field to use Dicom visualization instead of the already created File field. Just add the `dcm` extension into the field settings and module will do the rest.

## Table of contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Requirements

This module requires:
* Drupal core module "File"


## Installation
Install as you would normally install a contributed Drupal module. Visit:
https://www.drupal.org/docs/extending-drupal/installing-drupal-modules for further information.


## Configuration
All configuration is available via `/admin/dicom-configuration`. More than 45 dicom tags are available to configure on this module. For eg. Patient ID, Patient's Name, Patient's Birth Date, Institution Name etc. You can set up:
- Add Prefix, Enable tag, Configure display order, Tag display position for each Dicom tags
- Tag color on the dicom visualization
- Enable/Disable dicom zoom on mouse scroll


# Maintainers
-----------
* [Sujan Shrestha (sujan-shrestha)](https://www.drupal.org/u/sujan-shrestha)
