  config = window.customWebWorkerConfig;

  cornerstoneWADOImageLoader.external.cornerstone = cornerstone;
  cornerstoneWADOImageLoader.webWorkerManager.initialize(config);

  let zoomDicom = drupalSettings.zoomDicom;
  let dicom_tag_color = drupalSettings.dicom_tag_color;

  function loadAndViewImage(imageId, id) {
    var element = document.getElementById('dicomImage-'+id);
    try {
        var start = new Date().getTime();
        cornerstone.loadAndCacheImage(imageId).then(function(image) {

            var tags_config = drupalSettings.tags_config;

            var top = 0;
            if (tags_config['top_left'] != undefined) {
                var i = 0;
                tags_config['top_left'].forEach(function(tagDetails) {
                    var patientID = image.data.string('x'+tagDetails.tag);
                    var prefix = tagDetails.prefix;
                    if(patientID != undefined) {
                        top = top + 20;
                        var idOverlay = document.createElement('div');
                        idOverlay.className = 'cornerstone-overlay';
                        idOverlay.style.position = 'absolute';
                        idOverlay.style.top = top+'px';
                        idOverlay.style.left = '10px';
                        idOverlay.style.color = dicom_tag_color;
                        idOverlay.style.fontFamily = 'Arial';
                        idOverlay.style.fontSize = '16px';

                        idOverlay.textContent = prefix + patientID;
                        element.appendChild(idOverlay);
                        i++;
                    }
                });
            }

            var top = 0;
            if (tags_config['top_right'] != undefined) {
                var i = 0;
                tags_config['top_right'].forEach(function(tagDetails) {
                    var patientID = image.data.string('x'+tagDetails.tag);
                    var prefix = tagDetails.prefix;
                    if(patientID != undefined) {
                        top = top + 20;
                        var idOverlay = document.createElement('div');
                        idOverlay.className = 'cornerstone-overlay';
                        idOverlay.style.position = 'absolute';
                        idOverlay.style.top = top+'px';
                        idOverlay.style.right = '10px';
                        idOverlay.style.color = dicom_tag_color;
                        idOverlay.style.fontFamily = 'Arial';
                        idOverlay.style.fontSize = '16px';
                        idOverlay.textContent = prefix + patientID;
                        element.appendChild(idOverlay);
                        i++;
                    }
                });
            }

            var top = 0;
            if (tags_config['bottom_left'] != undefined) {
                tags_config['bottom_left'].forEach(function(tagDetails, index) {
                    var idOverlay = document.createElement('div');
                    var patientID = image.data.string('x'+tagDetails.tag);
                    var prefix = tagDetails.prefix;
                    if(patientID != undefined) {
                        top = top + 20;

                        idOverlay.className = 'cornerstone-overlay';
                        idOverlay.style.position = 'absolute';
                        idOverlay.style.bottom = top+'px';
                        idOverlay.style.left = '10px';
                        idOverlay.style.color = dicom_tag_color;
                        idOverlay.style.fontFamily = 'Arial';
                        idOverlay.style.fontSize = '16px';
                        idOverlay.textContent = prefix + patientID;
                        element.appendChild(idOverlay);
                        i++;
                    }
                });
            }

            var top = 0;
            if (tags_config['bottom_right'] != undefined) {
                tags_config['bottom_right'].forEach(function(tagDetails, index) {
                    var idOverlay = document.createElement('div');
                    var patientID = image.data.string('x'+tagDetails.tag);
                    var prefix = tagDetails.prefix;
                    if(patientID != undefined) {
                        top = top + 20;
                        idOverlay.className = 'cornerstone-overlay';
                        idOverlay.style.position = 'absolute';
                        idOverlay.style.bottom = top+'px';
                        idOverlay.style.right = '10px';
                        idOverlay.style.color = dicom_tag_color;
                        idOverlay.style.fontFamily = 'Arial';
                        idOverlay.style.fontSize = '16px';
                        idOverlay.textContent = prefix + patientID;
                        element.appendChild(idOverlay);
                        i++;
                    }
                });
            }

            var viewport = cornerstone.getDefaultViewportForImage(element, image);
            cornerstone.displayImage(element, image, viewport);

            // to zoom
            if(zoomDicom == true) {
                cornerstoneTools.mouseInput.enable(element);
                cornerstoneTools.mouseWheelInput.enable(element);
                cornerstoneTools.wwwc.activate(element, 1);
                // ww/wc is the default tool for left mouse button
                cornerstoneTools.pan.activate(element, 2);
                // pan is the default tool for middle mouse button
                cornerstoneTools.zoom.activate(element, 4);
                // zoom is the default tool for right mouse button
                cornerstoneTools.zoomWheel.activate(element);
                // zoom is the default tool for middle mouse wheel
            }

        }, function(err) {
         console.log('Report not found!');
     });
}
catch(err) {
    console.log('Report not found!');
}
}

visualizeDicomFiles();
function visualizeDicomFiles(){
    var elementsArray = [...document.getElementsByClassName('wadoURL')];
    for (var i = 0; i < elementsArray.length; i++) {
      var element3 = elementsArray[i];
      var url = element3.dataset.url;
      var id = element3.dataset.id;
      url = "wadouri:" + url;
      loadAndViewImage(url, id);
      var element5 = document.getElementById('dicomImage-'+id);
      cornerstone.enable(element5);
  }
}
