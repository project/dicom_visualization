<?php

namespace Drupal\dicom_visualization\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Klavio marketing settings for this site.
 */
class DicomConfiguration extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $dicom_tags = dicom_tags();
    $count_dicom_tags = count($dicom_tags);

    $top_left_tags = $this->config('dicom_visualization.settings')->get('top_left_tags')??[];
    $top_right_tags = $this->config('dicom_visualization.settings')->get('top_right_tags')??[];
    $bottom_left_tags = $this->config('dicom_visualization.settings')->get('bottom_left_tags')??[];
    $bottom_right_tags = $this->config('dicom_visualization.settings')->get('bottom_right_tags')??[];

    foreach ($dicom_tags as $dkey => $dicom_tag) {
      $tag = $dicom_tag['tag'];
      $tag_name = $dicom_tag['name'];
      $tag_description = $dicom_tag['description'];
      $enabled = $this->config('dicom_visualization.settings')->get($tag.'_enabled')??false;
      $position = $this->config('dicom_visualization.settings')->get($tag.'_position')??'';

      $tag_position = '';

      if(in_array($tag, $top_left_tags)) {
        $tag_position = $this->t('<small>(Top left)</small>');

      } elseif(in_array($tag, $top_right_tags)) {
        $tag_position = $this->t('<small>(Top right)</small>');

      } elseif(in_array($tag, $bottom_left_tags)) {
        $tag_position = $this->t('<small>(Bottom left)</small>');

      } elseif(in_array($tag, $bottom_right_tags)) {
        $tag_position = $this->t('<small>(Bottom right)</small>');
      }

      if(!$enabled && $position != '') {
        $tag_position = '<s>'.$tag_position.'</s>';
      }

      $form['dicom_'.$tag] = [
        '#type' => 'details',
        '#title' => $this->t($tag_name).' '.$tag_position,
        '#description' => $this->t($tag_description).' <small>('.$tag.')</small>',
      ];
      $form['dicom_'.$tag][$tag.'_prefix'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Prefix'),
        '#default_value' => $this->config('dicom_visualization.settings')->get($tag.'_prefix'),

      ];
      $form['dicom_'.$tag][$tag.'_enabled'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable'),
        '#default_value' => $enabled,
      ];
      $form['dicom_'.$tag][$tag.'_order'] = [
        '#type' => 'number',
        '#title' => $this->t('Tag display order'),
        '#min' => 0,
        '#max' => $count_dicom_tags,
        '#default_value' => $this->config('dicom_visualization.settings')->get($tag.'_order')??$dkey,
      ];
      $form['dicom_'.$tag][$tag.'_position'] = [
        '#type' => 'select',
        '#title' => $this->t('Tag display position'),
        '#options' => dicom_tag_display_positions(),
        '#default_value' => $position,
      ];
    }

    $form['dicom_zoom'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable zoom'),
      '#default_value' => $this->config('dicom_visualization.settings')->get('dicom_zoom')??true,
    ];

    $form['dicom_tag_color'] = [
      '#type' => 'color',
      '#title' => $this->t('Tag text color'),
      '#default_value' => $this->config('dicom_visualization.settings')->get('dicom_tag_color')??'white',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['dicom_visualization.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'dicom_visualization_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $dicom_tags = dicom_tags();
    $top_left_tags = array();
    $top_right_tags = array();
    $bottom_left_tags = array();
    $bottom_right_tags = array();

    $config = $this->config('dicom_visualization.settings');
    foreach ($dicom_tags as $dkey => $dicom_tag) {
      $tag = $dicom_tag['tag'];
      $tag_position = $form_state->getValue($tag.'_position');
      if($tag_position != '') {
        $enabled = $form_state->getValue($tag.'_enabled');
      } else {
        $enabled = false;
      }

      $config->set($tag.'_prefix', $form_state->getValue($tag.'_prefix'))
      ->set($tag.'_enabled', $enabled)
      ->set($tag.'_order', $form_state->getValue($tag.'_order'))
      ->set($tag.'_position', $tag_position);

      switch ($tag_position) {
        case 'top_left':
        $top_left_tags[] = $tag;

        break;

        case 'top_right':
        $top_right_tags[] = $tag;

        break;

        case 'bottom_left':
        $bottom_left_tags[] = $tag;

        break;

        case 'bottom_right':
        $bottom_right_tags[] = $tag;

        break;
      }

      $config->set('top_left_tags', $top_left_tags)
      ->set('top_right_tags', $top_right_tags)
      ->set('bottom_left_tags', $bottom_left_tags)
      ->set('bottom_right_tags', $bottom_right_tags);
    }

    $dicom_zoom = $form_state->getValue('dicom_zoom');
    $dicom_tag_color = $form_state->getValue('dicom_tag_color');
    $config->set('dicom_zoom', $dicom_zoom);
    $config->set('dicom_tag_color', $dicom_tag_color);

    $config->save();
    parent::submitForm($form, $form_state);
  }



}
