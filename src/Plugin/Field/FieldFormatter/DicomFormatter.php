<?php

namespace Drupal\dicom_visualization\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'dicom_file_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "dicom_file_formatter",
 *   label = @Translation("Dicom File Formatter"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class DicomFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $config = \Drupal::config('dicom_visualization.settings');
    $dicom_zoom = $config->get('dicom_zoom')??true;
    $dicom_tag_color = $config->get('dicom_tag_color')??'#fff';

    $positions = array(
      'top_left',
      'top_right',
      'bottom_left',
      'bottom_right',
    );

    $tags_config = array();

    foreach ($positions as $position) {
      $tags = $position.'_tags';
      $tags_data =  $config->get($tags)??[];
      if(!empty($tags_data)) {
        foreach ($tags_data as $tag) {
          $position = $config->get($tag.'_position');
          $enabled = $config->get($tag.'_enabled');
          if($enabled && $position != '') {
            $data['tag'] = $tag;
            $data['position'] = $position;
            $data['enabled'] = $enabled;
            $data['prefix'] = $config->get($tag.'_prefix');
            $data['order'] = $config->get($tag.'_order');

            $tags_config[$position][] = $data;
          }
        }
      }
    }

    if(!empty($tags_config)) {
      foreach ($tags_config as $position => $tags) {
        usort($tags, 'reorder_dicom_tags');
        $tags_config[$position] = $tags;
      }
    }

    if(!empty($tags_config['bottom_left'])) {
      $tags_config['bottom_left'] = reverse_array_data($tags_config['bottom_left']);
    }

    if(!empty($tags_config['bottom_right'])) {
      $tags_config['bottom_right'] = reverse_array_data($tags_config['bottom_right']);
    }


    foreach ($items as $delta => $item) {
      // Build your custom render array for each file item.
      $file = $item->entity;
      if($file) {
        $full_url = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());

        $template_vars = [
          '#theme' => 'dicom_file_template',
          '#file' => $file,
          '#full_url' => $full_url,
          '#tags_config' => $tags_config,
          '#zoomDicom' => $dicom_zoom,
          '#dicom_tag_color' => $dicom_tag_color,
        ];

        $elements[$delta] = $template_vars;
      }
    }

    $elements['#attached']['library'][] = 'dicom_visualization/dicom-file-formatter';
    $elements['#attached']['drupalSettings']['tags_config'] = $tags_config;
    $elements['#attached']['drupalSettings']['zoomDicom'] = $dicom_zoom;
    $elements['#attached']['drupalSettings']['dicom_tag_color'] = $dicom_tag_color;

    return $elements;
  }

}
