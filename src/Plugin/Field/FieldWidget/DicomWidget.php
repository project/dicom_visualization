<?php

namespace Drupal\dicom_visualization\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'dicom_visualization' widget.
 *
 * @FieldWidget(
 *   id = "dicom_visualization",
 *   label = @Translation("Dicom File Widget"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class DicomWidget extends FileWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'additional_setting' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['additional_setting'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional Setting'),
      '#default_value' => $this->getSetting('additional_setting'),
      '#description' => $this->t('An additional setting for the custom widget.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function widgetFormElements(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $elements = parent::widgetFormElements($items, $delta, $element, $form, $form_state);

    // Add custom form elements or modify existing ones.
    // For example, you can add an additional setting to the file widget form.
    $elements['#additional_setting'] = $this->getSetting('additional_setting');

    return $elements;
  }

}
